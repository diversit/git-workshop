package com.bol.workshop.git;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( new App().sayHelloTo("world") );
    }

    public String sayHelloTo(String name) {
        return "Hello " + name;
    }
}
