package com.bol.workshop.git;


import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AppTest {

    @Test
    public void sayHelloToShouldPrependNameWithHello()
    {
        App app = new App();
        assertThat(app.sayHelloTo("World"), is("Hallo World"));
    }
}
